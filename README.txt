CSS Reset

BACKGROUND

Themers are often under immense pressure to ensure that all browsers render
things in a consistent way. This is complicated by browsers having their own 
default values for various html elements. This obstacle can be easily 
overcome by using a css reset as first outlined by Erik Meyer.

ABOUT

This is a simple module that adds a css reset at a system level. One benefit 
of this appoach is that the 'reset' can be loaded before other modules that 
include some css etc. This means that the 'reset' should only normalise the 
user agent's styles and not styles that have been defined in other core and 
contributed modules that may be loaded by Drupal before the theme layer.

INSTALLATION

Install like any other Drupal module.

Once installed create a folder called 'css_reset' in the 'libraries' folder 
and copy the meyer reset there. By default I expect it to be called reset.css

ENABLE

There is an admin UI at

/admin/config/media/css_reset

This will in future support enabling and disabling the module.

TEST

Once installed you should see that the css 'reset' is the first stylesheet to 
load.

TODO

Add support for other css resets.
Add ability to switch on and off from the admin ui.

RESOURCES
http://meyerweb.com/eric/tools/css/reset/
