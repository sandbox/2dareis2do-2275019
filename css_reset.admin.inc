<?php

/**
 * @file
 * Admin only functionality for the reset module.
 */

/**
 * Menu form callback; Display the reset admin form.
 */
function css_reset_admin_form() {
  $form = array();

  // Detect if the library is available and display a message if it is not.
  $libraries = libraries_get_libraries();
  if (!isset($libraries['css_reset'])) {
    $link = l(
      t('CSS reset'),
      'http://www.cssreset.com/scripts/eric-meyer-reset-css/',
        array(
          'attributes' => array(
            'target' => '_blank',
            'external' => TRUE,
            'class' => array(
              'css-reset-link',
              'external',
            ),
          ),
        ));
    $form['message'] = array(
      '#markup' => '<p>' . t('The "css reset" of your choice is not installed! For your convenience a copy of the "meyer reset" has been bundled with this module. To install simply copy the folder called "resets" to you libraries folder and rename "css_reset". Alternatively, you can also download a copy of meyer from !url or perhaps you would prefer to use another one of your choice? If so, simply place it in the css_reset libraries folder with the name "reset.css". For experienced themers, why not try writing your own?',
        array('!url' => $link)) . '</p>',
    );
  }

  $form['css_reset_sitewide'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Reset library site wide'),
    '#default_value' => variable_get('css_reset_sitewide', FALSE),
  );

  return system_settings_form($form);
}
